<?php

class HomeController {

    public function index($request, $response) {
        return $response->view('home.index');
    }
}