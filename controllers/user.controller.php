<?php

include 'models/user.model.php';
use Model\User;

class UserController {
    private $user;

    function __construct() {
        $this->user = new User;
    }

    
    public function index($request, $response) {
        $data['users'] = $this->user->get();
        return $response->view('user.index', $data);
    }

    public function edit($request, $response) {
        $user_id = $request->params('id');
        $match_user = $this->user->find($user_id);
        
        return $response->view('user.edit', $match_user);
    }

    public function delete($request, $response) {
        $user_id = $request->params('id');
        $match_user = $this->user->where(['id' =>$user_id])->delete();
        $users = $this->user->all();
        
        $response->redirect('/users')->flush(['success' => 'User was deleted successfully.']);
    }

    public function update($request, $response) {
        $user_id = $request->params('id');
        $user = new User;
        $body = $request->body();
        $user->where(['id' => $user_id])
        ->update([
            'first_name' => $request->body('first_name'),
            'last_name' => $request->body('last_name'),
            'email' => $request->body('email'),
            'password' => $request->body('password')
        ]);

        $updated_user = $user->find($user_id);
        $response->redirect("/users/$user_id/edit")->flush(['success' => 'User udpated successfully']);
        
    }

    public function create($request, $response) {
        return $response->view('user.create');
    }

    public function store($request, $response) {
        if(!$this->isValidRequired($request->body())) {
            return $response->redirect('/users/create')->flush(['error' => 'All fields should be filled out.']);
        }


        $this->user->create([
            'first_name' => $request->body('first_name'),
            'last_name' => $request->body('last_name'),
            'email' => $request->body('email'),
            'password' => $request->body('password'),
        ]);

        return $response->redirect('/users')->flush(['success' => 'User was created successfully.']);
    }

    protected function isValidRequired($data) {
        $empty_count = 0;
        foreach($data as $key => $value) {
            if(!trim($value)) {
                $empty_count ++;
            }
        }

        return $empty_count == 0;
    }
}