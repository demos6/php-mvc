<?php
session_start();

$GLOBALS['ROUTE_PATH'] = $_GET['url'] == 'index.php'? '/': $_GET['url'];

include_once 'core/helpers/global.helper.php';
include 'core/includes/route.inc.php';
include 'core/includes/response.inc.php';
include 'core/includes/request.inc.php';
include_once 'autoload.php';