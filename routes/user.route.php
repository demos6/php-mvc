<?php

Route::get('users', 'UserController', 'index');
Route::get('users/create', 'UserController', 'create');
Route::post('users', 'UserController', 'store');
Route::get('users/:id/edit', 'UserController', 'edit');
Route::put('users/:id', 'UserController', 'update');
Route::delete('users/:id', 'UserController', 'delete');