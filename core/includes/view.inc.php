<?php

$directory = 'views/';
$it = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($directory));
while($it->valid()) {
    
    if (!$it->isDot()) {
        include $it->key();
    }

    $it->next();
}