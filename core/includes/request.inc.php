<?php

// echo $GLOBALS['ROUTE'];

class Request {
    private $route;
    private $current_route;

    function __construct($route) {
        $this->route = $route;
        $this->current_route = $GLOBALS['ROUTE_PATH'];
    }

    public function params($key = '') {
        $param_items = [];

        $route_array = explode('/', trim($this->route, '/'));
        $current_route_array = explode('/', trim($this->current_route , '/'));

        if((count($route_array) == count($current_route_array))) {
            for($i = 0; $i < count($route_array); $i++) {
                $route_array_item = $route_array[$i];
                if((substr($route_array_item,0,1) == ':' && strlen($route_array_item)>1)) {
                    $param_key = substr($route_array_item,1);
                    $param_items[$param_key] = $current_route_array[$i];
                }
            }        
        }

        if($key) {
            return isset($param_items[$key])? $param_items[$key]: '';
        }

        return $param_items;
    }

    public function body($key = '') {
        $body_items = array_merge($_POST, $_GET);
        unset($body_items['_method']);

        if($key) {
            return $body_items[$key];
        }

        return $body_items;
    }
}