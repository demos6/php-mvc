<?php

class Response {

    public function view($viewPath, $data = []) {
        if(is_array($data) && count($data)>0) {
            extract($data);
        }
        
        $path = explode('.', $viewPath);
        $readablePath = implode('/', $path).'.view.php';
        $file = 'views/'.$readablePath;

        $template = file_get_contents($file);
        $updated_template = str_replace('{{', '<?=',$template);
        $updated_template = str_replace('}}', '?>',$updated_template);
        file_put_contents($file, $updated_template);
        include $file;
        file_put_contents($file, $template);
        
        session_unset();
    }

    public function flush($data) {
        foreach($data as $key => $value) {
            $_SESSION[$key] = $value;
        }
    }

    public function redirect($url) {
        header("Location: $url");
        return $this;
    }
}