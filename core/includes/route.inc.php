<?php

class Route {
    public static function get(...$args) {
        self::processRoute($args, __FUNCTION__);
    }

    public static function post(...$args) {
        self::processRoute($args, __FUNCTION__);
    }

    public static function patch(...$args) {
        self::processRoute($args, __FUNCTION__);
    }


    public static function put(...$args) {
        self::processRoute($args, __FUNCTION__);
    }

    public static function delete(...$args) {
        self::processRoute($args, __FUNCTION__);
    }

    private static function isMatchRoute($route, $function) {
        $current_method = strtolower($_SERVER['REQUEST_METHOD']);
        if($function != $current_method) {
            return false;
        }

        $current_route = $GLOBALS['ROUTE_PATH'];
        $route_array = explode('/', trim($route, '/'));
        $current_route_array = explode('/', trim($current_route, '/'));

        $route_match = false;

        if($route_array == $current_route_array) {
            $route_match = true;
        }

        if((count($route_array) == count($current_route_array)) && !$route_match) {
            $match = 0;
            for($i = 0; $i < count($route_array); $i++) {
                $route_array_item = $route_array[$i];
                if(
                    ($route_array_item == $current_route_array[$i]) ||
                    (substr($route_array_item,0,1) == ':' && strlen($route_array_item)>1)
                ) {
                    $match++;
                }
            }

            $route_match = count($route_array) === $match;
        }


        return $route_match && $function == $current_method;
    }

    private static function processRoute($args, $function) {
        [$route, $className, $method] = $args;
        

        if(self::isMatchRoute($route, $function)) {  
            $classPath = explode('/', $className);
            $classController = end($classPath);
            array_pop($classPath);
            
            $filePath = '';
            if(count($classPath) > 0) {
                $filePath = '/'.implode('/', $classPath);
            }

            foreach (glob("controllers$filePath/*.controller.php") as $filename) {
                include $filename;
            }

            $request = new Request($route);
            $response = new Response;

            $controller = new $classController;
            echo $controller->$method($request, $response);
        } else {
            return 'Route Not Found';
            exit();
        }
    }
}

if(isset($_POST['_method']) 
    && in_array(strtoupper($_POST['_method']), ['PUT','PATCH','DELETE'])
) {
    $_SERVER['REQUEST_METHOD'] = $_POST['_method'];
}