<?php

class DB {
    protected $tableName;

    private $filtered_data;
    private $all_data;

    function __construct() {
        $this->all_data = $this->all();
        $this->filtered_data = $this->all();
    }

    protected function connect() {
        $servername = getEnv('DB_HOST');;
        $username = getEnv('DB_USERNAME');
        $password = getEnv('DB_PASSWORD');
        $dbname = getEnv('DB_NAME');

        return new mysqli($servername, $username, $password, $dbname);
    }

    protected function table($tableName) {
        $this->tableName = $tableName;
        return $this;
    }

    public function where($args = []) {
        $this->setFilteredData($args);

        return $this;
    }

    protected function setFilteredData($args) {
        $this->filtered_data = array_filter($this->all_data, function ($data) use ($args){
            $match = 0;
            foreach($args as $key => $value) {
                if(isset($data[$key])) {
                    $column_value = $data[$key];

                    if(is_array($value) && in_array($column_value, $value)) {
                        $match ++;
                    }else if($column_value == $value) {
                        $match ++;
                    }

                }
            }

            return $match == count($args);
        });
    }

    protected function getFilteredData() {
        return $this->filtered_data;
    }

    public function all() {
        $sql = "SELECT * FROM ".$this->tableName;
        $result = $this->connect()->query($sql);

        $data = [];
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $data[] = $row;
            }
        }

        $this->all_data = $data;
        $this->connect()->close();
        return $data;
    }

    public function create($data) {

        $columns = array_keys($data);
        $values = array_values($data);
        $values_text = implode("','", $values);
        $columns_text = implode(",", $columns);

        if(count($columns)>0 && count($values)>=count($columns)) {
            $sql = "INSERT INTO ".$this->tableName." ($columns_text) VALUES ('$values_text')";
            if ($this->connect()->query($sql) === TRUE) {
                return $this->connect()->insert_id;
            } else {
                echo "Error: " . $sql . "<br>" . $this->connect()->error;
                return $this->connect()->insert_id;
            }

            $this->connect()->close();
        }
    }

    public function update($data) {
        if(count($data) == 0) {
            return;
        }

        $columns_values = [];
        foreach($data as $key => $value) {
            $columns_values[] = "$key='$value'";
        }

        $values_to_update = implode(', ', $columns_values);

        $updated_count = 0;
        foreach($this->filtered_data as $data) {
            $sql = "UPDATE ".$this->tableName." SET $values_to_update WHERE ID='".$data['id']."'";

            if ($this->connect()->query($sql) === TRUE) {
                $updated_count ++;
            }
        }

        $this->connect()->close();
        return $updated_count;
    }


    public function delete() {
        $item_deleted_count = 0;
        foreach($this->filtered_data as $data) {
            $sql = "DELETE FROM ".$this->tableName." WHERE ID='".$data['id']."'";
            if ($this->connect()->query($sql) === TRUE) {
                $item_deleted_count ++;
            }
        }

        $this->connect()->close();
        return $item_deleted_count;
    }

    public function get() {
        return $this->getFilteredData();
    }

    public function find($id) {
        $this->setFilteredData(['id' => $id]);

        $records = array_values($this->getFilteredData());
        return count($records)>=1? $records[0]: null;
    }
}
